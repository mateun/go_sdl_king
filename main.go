package main

import "github.com/veandco/go-sdl2/sdl"



func main()  {
	if err := sdl.Init(sdl.INIT_VIDEO); err != nil { panic(err)}
	defer sdl.Quit()

	sdl.Log("init ok")

	window, err := sdl.CreateWindow("test", 200, 300, 800, 600, sdl.WINDOW_SHOWN)
	if (err != nil) {
		panic(err)
	}
	defer window.Destroy()

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		panic(err)
	}
	defer renderer.Destroy()

	surface, err := window.GetSurface()
	if (err != nil) {
		panic(err)
	}
	surface.FillRect(nil, 0)
	rect := sdl.Rect{0, 0,  64, 64 }
	surface.FillRect(&rect, 0xffff0000)
	window.UpdateSurface()
	playerTexture, err := renderer.CreateTextureFromSurface(surface)
	if err != nil { panic(err) }

	playerPosX := 5.0

	running := true
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
				case *sdl.QuitEvent:
					println("Quit")
					running = false
					break
			}

		}

			playerPosX += 0.01

			playerRect := sdl.Rect{ int32(playerPosX), 300, 64, 64}

			renderer.Clear()
			renderer.Copy(playerTexture, nil, &playerRect)
			renderer.Present()
	}





}
