# GO SDL King

This is a small strategy game based on the GO language with an SDL2 binding. 

### Running

Please copy an SDL2.dll to the execution directory. 

``` 
go run main.go
```
